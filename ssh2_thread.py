#-*- coding: utf-8 -*-
#!/usr/bin/python
import paramiko
import threading

def ssh2(ip, username, passwd, cmd):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, 22, username, passwd, timeout=5)
        for m in cmd:
            studin, stdout, stderr = ssh.exec_command(m)
#           stdin.write("Y")
            out = stdout.readlines()
            for o in out:
                print o,
            print '%s\tOK\n' %(ip) 
        ssh.close()
    except:
        print '%s\tError\n' %(ip)

if __name__ == '__main__':
    cmd = ['cal', 'echo hello']
    username = "root"
    passwd = "123456"
    ip=['10.33.0.185', '10.33.1.241']
    print "Begin......"
#    ssh1(ip, username, passwd, cmd)          
    for i in ip:
        a = threading.Thread(target=ssh2, args=(i, username, passwd, cmd))
        a.start()
