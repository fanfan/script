#-*- coding: utf8 -*-
#/usr/bin/python

import MySQLdb
conn = MySQLdb.connect(host="localhost", user="root", passwd="")

cursor = conn.cursor()
sql1 = "drop database if exists devops "
sql2 =  "create database devops character set utf8 collate utf8_bin"
s1 = cursor.execute(sql1)
s2 = cursor.execute(sql2)
print s1
print s2
sql3 = "use devops"
s3 = cursor.execute(sql3)
print s3
sql4 = "create table member(mid int(8) primary key auto_increment, host varchar(15), port int(5), username varchar(50), password varchar(50), gid int(8) not null default 0)"
sql5 = "create table groups(gid int(8) primary key auto_increment, gname varchar(50))"
s4 = cursor.execute(sql4)
s5 = cursor.execute(sql5)
print s4
print s5

sql6 = "insert into member(host,port, username, password) values ('10.33.0.185', 22, 'root', '123456')"
s6 = cursor.execute(sql6)
print s6
