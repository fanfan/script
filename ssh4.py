#-*- coding: utf-8 -*-
#!/usr/bin/python
import paramiko

def ssh4(ip, port, username, passwd, cmd):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, port, username, passwd, timeout=5)
        for m in cmd:
            studin, stdout, stderr = ssh.exec_command(m)
            out = stdout.readlines()
            for o in out:
                print o,
            print '%s\tOK\n' %(ip) 
        ssh.close()
        return 0
    except:
        print '%s\tError\n' %(ip)
        return 1

if __name__ == '__main__':
    cmd = ['cal', 'echo hello']
    username = "root"
    passwd = "yuyao2014^&*"
    ip="60.12.222.185"
    port = 20997
    print "Begin......"
    ssh4(ip, port, username, passwd, cmd)
    
